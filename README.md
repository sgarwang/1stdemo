What: sgarwang's first demo project.
Date: 2015/12/10
-----------------------------------------

1.	Using gitlab:
	a. Sign up an account.
	b. Create this 1stdemo project.
	c. Setup your local client git environment
	d. Prepare your local ssh keypair.
	e. Upload your ssh public key:
		- https://gitlab.com/profile/keys
	f. Follow steps to clone and commit:
		- https://gitlab.com/sgarwang/1stdemo
		- git config --global user.name "sgarwang"
		- git config --global user.email "sgarwang@gmail.com"
		- cd /be-git/tiac/gitlab
		- git clone git@gitlab.com:sgarwang/1stdemo.git
			; OR 'git@gitlab.com:sgarwang/1stdemo.git
			; Both are SSH way (https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols)
		- cd 1stdemo
		- touch README.md
		- git add README.md
		- git commit -m "add README"
		- git push -u origin master
	x. References:
		- https://www.digitalocean.com/community/tutorials/how-to-use-the-gitlab-user-interface-to-manage-projects

2.  AD integration
    a. SSSD
        - https://fedorahosted.org/sssd/wiki/Configuring_sssd_with_ad_server
    b. Identity Manager
        - https://technet.microsoft.com/en-gb/library/cc731178.aspx#BKMK_command
    c. Centos 6 no Kerberos
        - http://htfdidt.blogspot.tw/2014/06/centos-6-with-active-directory.html
    d. MongoDB AD
        - https://docs.mongodb.org/manual/tutorial/configure-ldap-sasl-activedirectory/
    e. Cryus-SASL2
        - http://www.rhyous.com/2009/11/10/how-to-configure-subversion-to-use-cyrus-sasl2-authentication-to-authenticate-to-a-mysql-database/
    f. Subversion with SASL
        - https://community.bitnami.com/t/svn-repo-access-problems-with-sasl-authentication/36220
    g. Svnserve
        - http://svnbook.red-bean.com/nightly/en/svn.serverconfig.svnserve.html#svn.serverconfig.svnserve.sasl
    h. Dhost
        - http://www.dghost.com/techno/internet/setting-up-a-subversion-server-on-linux-with-sasl-authentication-against-a-ldap-active-directory-database
    i. Stackoverflow
        - http://stackoverflow.com/questions/1794242/svn-sasl-activedirectory-how-to
3. Emulator
    - https://sites.google.com/site/upgradedgamingx/cl3l-l4vv35l_lmn355/pokemon-crystal-download
    -